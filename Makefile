CC		= gcc
CPP		= g++
PKGCONFIG	= pkg-config
STRIP		= strip
UIC		= /usr/lib/x86_64-linux-gnu/qt5/bin/uic
MOC		= /usr/lib/x86_64-linux-gnu/qt5/bin/moc
RCC		= /usr/lib/x86_64-linux-gnu/qt5/bin/rcc

CFLAGS		= -std=c11 -D_GNU_SOURCE
CPPFLAGS	= -std=c++11 -fPIC
LDFLAGS		= -pthread

BUILDFLAGS	= -O3
DEBUGFLAGS	= -D_FORTIFY_SOURCE=1 -Og -ggdb -fsanitize=address
PROFILEFLAGS	= -O3 -ggdb -pg

BINARY		= Balance

SRCDIR		= src
RESDIR		= res
DEPDIR		= dep
BUILDDIR	= build

PKGS		= Qt5Widgets Qt5Concurrent

IDEPS		= $(shell $(PKGCONFIG) --cflags $(PKGS)) -I. -I$(BUILDDIR) -I$(DEPDIR)/wiiuse/src -I$(DEPDIR)/apr/include
LDDEPS		= $(shell $(PKGCONFIG) --libs $(PKGS))

RESFILES	= $(wildcard $(RESDIR)/*.qrc)
UIFILES		= $(wildcard $(RESDIR)/*.ui)
MOCFILES	= $(wildcard $(SRCDIR)/*.hpp)
CFILES		= $(wildcard $(SRCDIR)/*.c)
CPPFILES	= $(wildcard $(SRCDIR)/*.cpp)
CPPFILES	+= $(patsubst $(RESDIR)/%.qrc,$(BUILDDIR)/ui_%.cpp,$(RESFILES))
CPPFILES	+= $(patsubst $(SRCDIR)/%.hpp,$(BUILDDIR)/ui_%.cpp,$(MOCFILES))

DEPS		= *.so

.PHONY: all
all: CFLAGS	+= $(BUILDFLAGS)
all: CPPFLAGS	+= $(BUILDFLAGS)
all: LDFLAGS	+= $(BUILDFLAGS)
all: binary
	@echo STRIP $(BUILDDIR)/$(BINARY)
	@$(STRIP) $(BUILDDIR)/$(BINARY)
	@echo STRIP $(BUILDDIR)/*.so
	@$(STRIP) $(BUILDDIR)/*.so

.PHONY: win32
win32: CC	= i686-w64-mingw32-gcc
win32: CPP	= i686-w64-mingw32-g++
win32: PKGCONFIG= i686-w64-mingw32-pkg-config
win32: STRIP	= i686-w64-mingw32-strip
win32: WINDRES	= i686-w64-mingw32-windres
win32: UIC	= i686-w64-mingw32-uic-qt5
win32: MOC	= i686-w64-mingw32-moc-qt5
win32: RCC	= i686-w64-mingw32-rcc-qt5
win32: CFLAGS	= -std=c11 -D_GNU_SOURCE -mwindows $(BUILDFLAGS)
win32: CPPFLAGS	= -std=c++11 -mwindows $(BUILDFLAGS)
win32: LDFLAGS	= -pthread -mwindows $(BUILDFLAGS) -L$(DEPDIR)/apr/.libs
win32: LDDEPS	= $(shell $(PKGCONFIG) --libs $(PKGS)) -lapr-1.dll
win32: DEPS	= *.dll
win32: BINARY	= Balance.exe
win32: c_objects cpp_objects deps
	@echo WINDRES $(BUILDDIR)/resources.res
	@$(WINDRES) $(RESDIR)/*.rc -O coff $(BUILDDIR)/resources.res
	@echo LD $(BUILDDIR)/$(BINARY)
	@$(CPP) $(LDFLAGS) $(BUILDDIR)/*.o $(BUILDDIR)/*.res -o $(BUILDDIR)/$(BINARY) $(LDDEPS) $(BUILDDIR)/$(DEPS)
	@echo STRIP $(BUILDDIR)/$(BINARY)
	@$(STRIP) $(BUILDDIR)/$(BINARY)
	@echo STRIP $(BUILDDIR)/*.dll
	@$(STRIP) $(BUILDDIR)/*.dll

.PHONY: debug
debug: CFLAGS	+= $(DEBUGFLAGS)
debug: CPPFLAGS	+= $(DEBUGFLAGS)
debug: LDFLAGS	+= $(DEBUGFLAGS)
debug: binary

.PHONY: profile
profile: CFLAGS		+= $(PROFILEFLAGS)
profile: CPPFLAGS	+= $(PROFILEFLAGS)
profile: LDFLAGS	+= $(PROFILEFLAGS)
profile: binary

.PHONY: binary
binary: c_objects cpp_objects deps
	@echo LD $(BUILDDIR)/$(BINARY)
	@$(CPP) $(LDFLAGS) $(BUILDDIR)/*.o -o $(BUILDDIR)/$(BINARY) $(LDDEPS) $(BUILDDIR)/$(DEPS)

.PHONY: c_objects
c_objects: $(CFILES)

$(CFILES): dir
	@echo CC $(patsubst $(SRCDIR)/%.c,$(BUILDDIR)/%.o,$@)
	@$(CC) $(CFLAGS) $(IDEPS) -c $@ -o $(patsubst $(SRCDIR)/%.c,$(BUILDDIR)/%.o,$@)

.PHONY: cpp_objects
cpp_objects: $(CPPFILES)

$(CPPFILES): uis mocs resources
	@echo CPP $(BUILDDIR)/$(notdir $(patsubst %.cpp,%.o,$@))
	@$(CPP) $(CPPFLAGS) $(IDEPS) -c $@ -o $(BUILDDIR)/$(notdir $(patsubst %.cpp,%.o,$@))

.PHONY: dir
deps: dir
	@echo Copying dependencies...
	@find $(DEPDIR) -iname "$(DEPS)" | xargs -i cp -u {} $(BUILDDIR)/

.PHONY: mocs
mocs: $(MOCFILES)

$(MOCFILES): dir
	@echo MOC $(patsubst $(SRCDIR)/%.hpp,$(BUILDDIR)/ui_%.cpp,$@)
	@$(MOC) $@ > $(patsubst $(SRCDIR)/%.hpp,$(BUILDDIR)/ui_%.cpp,$@)

.PHONY: uis
uis: $(UIFILES)

$(UIFILES): dir
	@echo UIC $(patsubst $(RESDIR)/%.ui,$(BUILDDIR)/ui_%.hpp,$@)
	@$(UIC) $@ > $(patsubst $(RESDIR)/%.ui,$(BUILDDIR)/ui_%.hpp,$@)

.PHONY: resources
resources: $(RESFILES)

$(RESFILES): dir
	@echo RCC $(patsubst $(RESDIR)/%.qrc,$(BUILDDIR)/ui_%.cpp,$@)
	@$(RCC) $@ > $(patsubst $(RESDIR)/%.qrc,$(BUILDDIR)/ui_%.cpp,$@)

.PHONY: dir
dir:
	@echo Creating build directory...
	@mkdir -p $(BUILDDIR)

.PHONY: clean
clean:
	@echo Cleaning...
	@rm -rf $(BUILDDIR)

