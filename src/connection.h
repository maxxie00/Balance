/*
 *  Balance
 *
 *  Copyright (C) 2016  Massimo Martelli <max213_88@hotmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef CONNECTION_H
#define CONNECTION_H

#include "shared.h"

#include <time.h>

#define CONNECTION_TIMEOUT 3
#define MIN_WORKING_WEIGHT 20.0f

enum connection_manager_errorcodes {
	CONNECTION_OK,
	CONNECTION_ERROR_INTERNAL,
	CONNECTION_ERROR_NOT_FOUND,
	CONNECTION_ERROR_NOT_CONNECTED,
	CONNECTION_ERROR_TIMEOUT
};

enum boards {
	FIRST_BOARD,
	SECOND_BOARD,
	LEFT_BOARD = FIRST_BOARD,
	RIGHT_BOARD = SECOND_BOARD
};

enum connection_status {
	STATUS_NOT_CONNECTED,
	STATUS_CONNECTED_FIRST,
	STATUS_CONNECTED_BOTH
};

#define MAX_BOARDS 2

enum sensors {
	TOP_LEFT,
	TOP_RIGHT,
	BOT_LEFT,
	BOT_RIGHT
};

#define SENSORS_COUNT 4

enum mode {
	SINGLE_MODE,
	DOUBLE_MODE
};

struct board_data {
	const float *data[MAX_BOARDS];
};

extern const char *connection_manager_errormessages[];
extern const char *connection_status_text[];

#ifdef __cplusplus
extern "C" {
#endif

	extern enum connection_manager_errorcodes connect_boards();
	extern enum connection_status __hot get_connection_status();
	extern void disconnect_boards();
	extern enum connection_status __hot
		get_boards_data(struct board_data *data);

#ifdef __cplusplus
}
#endif

#endif

