/*
 *  Balance
 *
 *  Copyright (C) 2016  Massimo Martelli <max213_88@hotmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SRC_MAIN_WINDOW_H
#define SRC_MAIN_WINDOW_H

#include "connection.h"
#include "ui_main_window.hpp"

#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>

#define STATUS_MESSAGE_DURATION 5000

namespace Ui {
	class main_window;
}

class main_window : public QMainWindow {
	Q_OBJECT

	public:
		explicit main_window(QWidget *parent = NULL);
		~main_window();

		enum mode get_current_mode() const;
		void set_current_mode(enum mode mode);
		void update_connection_status(enum connection_status status);

	protected slots:
		void on_action_about_triggered(bool checked = false);
		void on_action_single_triggered(bool checked = false);
		void on_action_double_triggered(bool checked = false);
		void on_action_sync_boards_triggered(bool checked = false);
		void on_action_toggle_cursor_triggered(bool checked = false);
		void on_action_toggle_values_triggered(bool checked = false);
		void on_action_toggle_axes_triggered(bool checked = false);
		void queue_status_message(const char *message);

	signals:
		void status_changed(const char *message);

	private:
		Ui::main_window *ui;
		enum mode current_mode;
		QLabel connection_status_message;
		enum connection_status connection_status;

		void task_connect_boards();
};

#endif

