/*
 *  Balance
 *
 *  Copyright (C) 2016  Massimo Martelli <max213_88@hotmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "shared.h"
#include "renderer.hpp"
#include "main_window.hpp"

#include <QTimer>
#include <QPainter>
#include <QtWidgets/QOpenGLWidget>

renderer::renderer(QWidget *parent) :
	QOpenGLWidget(parent),
	refresher(new QTimer(this)),
	show_cursor(true),
	show_values(true),
	show_axes(true)
{
	font.setPixelSize(68);
	font.setBold(true);
	QObject::connect(refresher, SIGNAL(timeout()), this, SLOT(repaint()));
	refresher->start(REFRESH_TIMER);
}

renderer::~renderer()
{
	delete refresher;
}

void __hot renderer::paintEvent(QPaintEvent *event)
{
	painter.begin(this);

	painter.setRenderHint(QPainter::Antialiasing);
	painter.fillRect(0, 0, width(), height(), Qt::black);

	enum mode mode = ((main_window*)parentWidget()->parentWidget())->get_current_mode();

	struct board_data board_data;
	struct board_data *data_ptr = &board_data;

	enum connection_status status = get_boards_data(data_ptr);

	if (unlikely(status == STATUS_NOT_CONNECTED)) {
		data_ptr = NULL;
	}

	((main_window*)parentWidget()->parentWidget())->update_connection_status(status);

	switch(mode) {
		case SINGLE_MODE:
			if (show_axes) {
				paint_background_single();
			}
			if (show_values) {
				draw_sensors_values_single(data_ptr);
			}
			if (show_cursor) {
				paint_cursor_single(data_ptr);
			}
			break;
		case DOUBLE_MODE:
			if (show_axes) {
				paint_background_double();
			}
			painter.setPen(Qt::green);
			painter.drawLine(width() / 2, 0, width() / 2, height());
			if (show_values) {
				draw_sensors_values_double(data_ptr);
			}
			if (show_cursor) {
				paint_cursor_double(data_ptr);
			}
			break;
	}

	painter.end();
}

void __hot renderer::paint_background_single()
{
	painter.setPen(Qt::white);
	painter.drawLine(0, height() / 2, width(), height() / 2);
	painter.drawLine(width() / 2, 0, width() / 2, height());
}

void __hot renderer::paint_background_double()
{
	painter.setPen(Qt::white);
	painter.drawLine(0, height() / 2, width(), height() / 2);
	painter.drawLine(width() / 4, 0, width() / 4, height());
	painter.drawLine(width() * 0.75f, 0, width() * 0.75f, height());
}

void renderer::resizeEvent(QResizeEvent * event)
{
	QOpenGLWidget::resizeEvent(event);
}

void __hot renderer::paint_cursor_single(const struct board_data *board_data)
{
	painter.setPen(Qt::red);
	painter.setBrush(Qt::red);
	QPoint coordinates;
	calculate_coordinates_single(&coordinates, board_data);
	painter.drawEllipse(coordinates, POINTER_RADIUS, POINTER_RADIUS);
}

void __hot renderer::calculate_coordinates_single(QPoint *coordinates,
	const struct board_data *board_data)
{
	QPointF cops = { 0.0f, 0.0f };

	if (likely(board_data))
		get_CoPs(&cops, board_data->data[FIRST_BOARD]);

	coordinates->setX((cops.x() + 1.0f) * width() / 2);
	coordinates->setY((-cops.y() + 1.0f) * height() / 2);
}

void __hot renderer::paint_cursor_double(const struct board_data *board_data)
{
	painter.setPen(Qt::red);
	painter.setBrush(Qt::red);
	QPoint coordinates[MAX_BOARDS];
	calculate_coordinates_double(coordinates, board_data);

	for(int i = 0; i < MAX_BOARDS; ++i)
		painter.drawEllipse(coordinates[i], POINTER_RADIUS,
			POINTER_RADIUS);
}

void __hot renderer::calculate_coordinates_double(QPoint *coordinates,
	const struct board_data *board_data)
{
	QPointF cops = { 0.0f, 0.0f };

	for(int i = 0; i < MAX_BOARDS; ++i) {
		if (likely(board_data))
			get_CoPs(&cops, board_data->data[i]);

		coordinates[i].setX((cops.y() + (i * 2 + 1)) * (width() / 4));
		coordinates[i].setY((cops.x() + 1.0f) * height() / 2);
	}

}

void __hot renderer::get_CoPs(QPointF *cops, const float *data)
{
	float sum = data[TOP_LEFT] + data[TOP_RIGHT] + data[BOT_LEFT] +
		data[BOT_RIGHT];

	if (unlikely(!sum)) {
		cops->setX(0.0f);
		cops->setY(0.0f);
		return;
	}

	cops->setX(((data[TOP_RIGHT] + data[BOT_RIGHT] -
		(data[TOP_LEFT] + data[BOT_LEFT])) / sum));
	cops->setY(((data[TOP_LEFT] + data[TOP_RIGHT] -
		(data[BOT_LEFT] + data[BOT_RIGHT])) / sum));
}

static Qt::Alignment sensors_values_alignment_single[SENSORS_COUNT] =
{
    (Qt::AlignTop | Qt::AlignLeft),
    (Qt::AlignTop | Qt::AlignRight),
    (Qt::AlignBottom | Qt::AlignLeft),
    (Qt::AlignBottom | Qt::AlignRight)
};

static Qt::Alignment sensors_values_alignment_double[SENSORS_COUNT] =
{
	(Qt::AlignTop | Qt::AlignRight),
	(Qt::AlignBottom | Qt::AlignRight),
	(Qt::AlignTop | Qt::AlignLeft),
	(Qt::AlignBottom | Qt::AlignLeft)
};

void __hot renderer::draw_sensors_values_single(const struct board_data *board_data)
{
	float total = 0.0f;

	if (likely(board_data)) {
		for(int i = 0; i < SENSORS_COUNT; ++i) {
			total += board_data->data[FIRST_BOARD][i];
		}
	}

	painter.setFont(font);
	painter.setPen(Qt::white);

	if (unlikely(total < MIN_WORKING_WEIGHT))
		painter.setPen(Qt::red);

	QString text;

	for(int i = 0; i < SENSORS_COUNT; ++i) {
		if (likely(board_data)) {
			text.setNum(board_data->data[FIRST_BOARD][i], 'f', 2);
		} else {
			text = "--";
		}

		painter.drawText(rect(), sensors_values_alignment_single[i], text);
	}
}

void __hot renderer::draw_sensors_values_double(const struct board_data *board_data)
{
	float total[MAX_BOARDS] = { 0.0f, 0.0f };

	if (likely(board_data)) {
		for(int i = 0; i < MAX_BOARDS; ++i) {
			for(int j = 0; j < SENSORS_COUNT; ++j) {
				total[i] += board_data->data[i][j];
			}
		}
	}

	painter.setFont(font);

	for(int i = 0; i < MAX_BOARDS; ++i) {
		painter.setPen(Qt::white);

		if (unlikely(total[i] < MIN_WORKING_WEIGHT))
			painter.setPen(Qt::red);

		QString text;
		QRect text_box(i * width() / 2, 0, width() / 2, height());

		for(int j = 0; j < SENSORS_COUNT; ++j) {
			if (likely(board_data)) {
				text.setNum(board_data->data[i][j], 'f', 2);
			} else {
				text = "--";
			}

			painter.drawText(text_box,
				sensors_values_alignment_double[j], text);
		}

	}
}

void renderer::set_cursor_visible(bool show)
{
	show_cursor = show;
}

void renderer::set_values_visible(bool show)
{
	show_values = show;
}

void renderer::set_axes_visible(bool show)
{
	show_axes = show;
}

