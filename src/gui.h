/*
 *  Balance
 *
 *  Copyright (C) 2016  Massimo Martelli <max213_88@hotmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef GUI_H
#define GUI_H

#include "connection.h"

#define ERRGUI -1

#ifdef __cplusplus
extern "C" {
#endif

	int init_gui(int argc, char **argv);
	enum mode get_current_mode();
	void set_current_mode(enum mode mode);

#ifdef __cplusplus
}
#endif

#endif

