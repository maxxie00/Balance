/*
 *  Balance
 *
 *  Copyright (C) 2016  Massimo Martelli <max213_88@hotmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, version 3 of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "shared.h"
#include "connection.h"

#include <assert.h>
#include <signal.h>
#include <wiiuse.h>
#include <stdbool.h>
#include <pthread.h>
#include <stdatomic.h>

static void *poll_boards(void *args);

static struct {
	struct wiimote_t **connections;
	pthread_mutex_t lock;
	atomic_bool polling;
	pthread_t poller_thread;
} balance_boards = {
	.connections = NULL,
	.lock = PTHREAD_MUTEX_INITIALIZER,
	.polling = ATOMIC_VAR_INIT(false)
};

const char *connection_manager_errormessages[] = {
	[CONNECTION_OK] 			= "Sincronizzazione Completata.",
	[CONNECTION_ERROR_INTERNAL]		= "Errore Bluetooth.",
	[CONNECTION_ERROR_NOT_FOUND]		= "Nessuna Bilancia trovata.",
	[CONNECTION_ERROR_NOT_CONNECTED]	= "Errore di comunicazione.",
	[CONNECTION_ERROR_TIMEOUT]		= "Timeout di connessione."
};

const char *connection_status_text[] = {
	"Nessuna Bilancia connessa.",
	"Prima Bilancia connessa.",
	"Prima e Seconda Bilancia connessa."
};

static void *poll_boards(void *args)
{
	while(likely(balance_boards.polling)) {
		wiiuse_poll(balance_boards.connections, MAX_BOARDS);
	}

	return NULL;
}

extern enum connection_manager_errorcodes connect_boards()
{
	enum connection_manager_errorcodes err = CONNECTION_OK;

	if (unlikely(balance_boards.connections))
		disconnect_boards();

	assert(balance_boards.connections = wiiuse_init(MAX_BOARDS));

	assert(!pthread_mutex_lock(&balance_boards.lock));

	int found = wiiuse_find(balance_boards.connections, MAX_BOARDS,
		CONNECTION_TIMEOUT);

	if (unlikely(!found)) {
		wiiuse_cleanup(balance_boards.connections, MAX_BOARDS);
		balance_boards.connections = NULL;
		err = CONNECTION_ERROR_NOT_FOUND;
		goto cleanup;
	}

	int connected = wiiuse_connect(balance_boards.connections, MAX_BOARDS);

	if (unlikely(connected != found))
		err = CONNECTION_ERROR_TIMEOUT;

	for(int i = 0; i < connected; ++i)
		wiiuse_set_leds(balance_boards.connections[i], WIIMOTE_LED_1);

	balance_boards.polling = true;
	assert(!pthread_create(&balance_boards.poller_thread, NULL, poll_boards,
		NULL));

cleanup:
	assert(!pthread_mutex_unlock(&balance_boards.lock));

	return err;
}

extern enum connection_status __pure __hot get_connection_status()
{
	if (!balance_boards.connections)
		return 0;

	size_t count = 0;

	for(int i = 0; i < MAX_BOARDS; ++i) {
		if (WIIMOTE_IS_CONNECTED(balance_boards.connections[i]) &&
			balance_boards.connections[i]->exp.type == EXP_WII_BOARD)
			++count;
	}

	return count;
}

extern void __exit disconnect_boards()
{
	if (unlikely(!balance_boards.connections))
		return;

	assert(!pthread_mutex_lock(&balance_boards.lock));

	balance_boards.polling = false;

	while(unlikely(!pthread_kill(balance_boards.poller_thread, 0)));

	wiiuse_cleanup(balance_boards.connections, MAX_BOARDS);

	balance_boards.connections = NULL;

	pthread_join(balance_boards.poller_thread, NULL);

	assert(!pthread_mutex_unlock(&balance_boards.lock));
}

extern enum connection_status __hot
	get_boards_data(struct board_data *board_data)
{
	if (unlikely(!balance_boards.connections))
		return STATUS_NOT_CONNECTED;

	for(int i = 0; i < MAX_BOARDS; ++i)
		board_data->data[i] = &balance_boards.connections[i]->exp.wb.tl;

	return get_connection_status();
}

