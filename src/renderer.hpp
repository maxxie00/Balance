/*
 *  Balance
 *
 *  Copyright (C) 2016  Massimo Martelli <max213_88@hotmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SRC_RENDERER_H
#define SRC_RENDERER_H

#include "shared.h"
#include "connection.h"

#include <QTimer>
#include <QPainter>
#include <QOpenGLWidget>

#define POINTER_RADIUS 11
#define REFRESH_TIMER 16

class renderer : public QOpenGLWidget {
	Q_OBJECT

	public:
		explicit renderer(QWidget *parent = NULL);
		~renderer();

		void set_cursor_visible(bool show);
		void set_values_visible(bool show);
		void set_axes_visible(bool show);

	protected slots:
		void __hot paintEvent(QPaintEvent *event);
		void resizeEvent(QResizeEvent *event);

	protected:
		void __hot paint_background_single();
		void __hot paint_background_double();
		void __hot paint_cursor_single(const struct board_data *board_data);
		void __hot paint_cursor_double(const struct board_data *board_data);
		void __hot calculate_coordinates_single(QPoint *coordinates,
			const struct board_data *board_data);
		void __hot calculate_coordinates_double(QPoint *coordinates,
			const struct board_data *board_data);
		void __hot draw_sensors_values_single(const struct board_data *board_data);
		void __hot draw_sensors_values_double(const struct board_data *board_data);

	private:
		bool show_cursor, show_values, show_axes;
		QPainter painter;
		QTimer *refresher;
		QFont font;

		void __hot get_CoPs(QPointF *cops, const float *data);
};

#endif

