/*
 *  Balance
 *
 *  Copyright (C) 2016  Massimo Martelli <max213_88@hotmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef REPLAY_H
#define REPLAY_H

#include "shared.h"
#include "connection.h"

#include <time.h>
#include <stdint.h>

#define MAGIC "BRS"
#define VERSION 1

enum replay_mode {
	REPLAY_NONE,
	REPLAY_READING,
	REPLAY_WRITING
};

enum replay_manager_errorcodes {
	REPLAY_OK,
	REPLAY_ERROR_BACKEND,
	REPLAY_ERROR_FILE_NOT_FOUND,
	REPLAY_ERROR_BAD_PERMISSION,
	REPLAY_ERROR_BAD_FILE_FORMAT,
	REPLAY_ERROR_UNEXPECTED_EOF,
	REPLAY_ERROR_INTERNAL
};

struct replay_data {
	time_t timestamp;
	float data[SENSORS_COUNT];
};

struct replay_header {
	uint8_t magic[3];
	uint32_t version;
	uint8_t mode;
	uint64_t count;
	struct replay_data stream[];
};

extern char *replay_manager_errormessages[];

#ifdef __cplusplus
extern "C" {
#endif

	extern enum replay_manager_errorcodes
		open_replay(const char *file_name, enum replay_mode mode);
	extern enum replay_manager_errorcodes __hot
		read_next_data_packet(enum boards id, const size_t index,
		struct replay_data *data);
	extern enum replay_manager_errorcodes __hot
		push_data_packet(enum boards id, struct replay_data *data);
	extern enum replay_mode get_current_replay_mode();
	extern void close_replay();

#ifdef __cplusplus
}
#endif

#endif

