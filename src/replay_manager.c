/*
 *  Balance
 *
 *  Copyright (C) 2016  Massimo Martelli <max213_88@hotmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "gui.h"
#include "replay.h"
#include "shared.h"

#include <assert.h>
#include <string.h>
#include <pthread.h>
#include <apr_mmap.h>
#include <apr_file_io.h>

static struct {
	pthread_mutex_t lock;
	struct replay_header *header;
	enum replay_mode current_mode;
	struct apr_pool_t *pool;
	struct apr_file_t *file;
	struct apr_mmap_t *mmap;
	uint64_t current_position;
} replay = {
	.lock = PTHREAD_MUTEX_INITIALIZER,
	.header = NULL,
	.pool = NULL,
	.file = NULL,
	.mmap = NULL,
	.current_position = 0
};

char *replay_manager_errormessages[] = {
	[REPLAY_OK] = "",
	[REPLAY_ERROR_BACKEND] = "Errore durante l'esecuzione dell'operazione.",
	[REPLAY_ERROR_FILE_NOT_FOUND] = "Impossibile trovare il file specificato.",
	[REPLAY_ERROR_BAD_PERMISSION] = "Permessi insufficienti per completare l'operazione.",
	[REPLAY_ERROR_BAD_FILE_FORMAT] = "Formato del file sconosciuto.",
	[REPLAY_ERROR_UNEXPECTED_EOF] = "Fine del file inattesa.",
	[REPLAY_ERROR_INTERNAL] = "Errore interno."
};

static void __init __cold initialise_backend()
{
	apr_initialize();
}

static void __exit __cold backend_cleanups()
{
	apr_terminate();
}

extern enum replay_manager_errorcodes open_replay(const char *file_name,
	enum replay_mode mode)
{
	assert(!pthread_mutex_lock(&replay.lock));

	enum replay_manager_errorcodes errorcode;

	replay.current_mode = mode;

	apr_status_t status;

	if (unlikely(apr_pool_create(&replay.pool, NULL) != APR_SUCCESS))
		return REPLAY_ERROR_BACKEND;

	struct {
		apr_int32_t file, mmap;
	} flags;

	if (mode == REPLAY_READING) {
		flags.file = APR_FOPEN_READ;
		flags.mmap = APR_MMAP_READ;
	} else {
		flags.file = APR_FOPEN_WRITE | APR_FOPEN_CREATE | APR_FOPEN_BINARY;
		flags.mmap = APR_MMAP_WRITE;
	}

	status = apr_file_open(&replay.file, file_name, flags.file,
		APR_FPROT_OS_DEFAULT, replay.pool);

	switch(status) {
		case APR_SUCCESS:
			break;
		case APR_EACCES:
			errorcode = REPLAY_ERROR_BAD_PERMISSION;
			goto cleanup_pool;
		case APR_EBADF:
		case APR_EBADPATH:
			errorcode = REPLAY_ERROR_FILE_NOT_FOUND;
			goto cleanup_pool;
		default:
			errorcode = REPLAY_ERROR_BACKEND;
			goto cleanup_pool;
	}

	apr_finfo_t file_info;

	if (unlikely(apr_file_info_get(&file_info, APR_FINFO_SIZE, replay.file) !=
		APR_SUCCESS)) {
		errorcode = REPLAY_ERROR_BACKEND;
		goto cleanup_file;
	}

	replay.header = replay.mmap->mm;

	memcpy(replay.header->magic, MAGIC, 3);
	replay.header->version = VERSION;
	replay.header->mode = get_current_mode();

	assert(!pthread_mutex_unlock(&replay.lock));

	return REPLAY_OK;

cleanup_file:
	apr_file_close(replay.file);
cleanup_pool:
	apr_pool_destroy(replay.pool);
	replay.pool = NULL;
	replay.file = NULL;
	replay.mmap = NULL;
	replay.header = NULL;

	assert(!pthread_mutex_unlock(&replay.lock));

	return errorcode;
}

extern enum replay_manager_errorcodes __hot read_next_data_packet(enum boards id,
	const size_t index, struct replay_data *data)
{
	if (index >= replay.header->count)
		return REPLAY_ERROR_BAD_FILE_FORMAT;

	memcpy(data, &replay.header->stream[(id + 1) * index], sizeof(*data));

	return REPLAY_OK;
}

extern enum replay_mode get_current_replay_mode()
{
	return replay.current_mode;
}

extern void close_replay()
{
	assert(!pthread_mutex_lock(&replay.lock));

	if (replay.current_mode == REPLAY_WRITING) {
		size_t size = sizeof(*replay.header) + replay.header->count *
			sizeof(struct replay_data);

		apr_file_trunc(replay.file, size);

		replay.header->count = replay.current_position;
	}

	apr_file_close(replay.file);

	assert(!pthread_mutex_unlock(&replay.lock));
}

extern enum replay_manager_errorcodes __hot push_data_packet(enum boards id,
	struct replay_data *data)
{
	if (!replay.header)
		return REPLAY_ERROR_INTERNAL;

	memcpy(&replay.header->stream[replay.current_position++], data, sizeof(*data));

	return REPLAY_OK;
}

