/*
 *  Balance
 *
 *  Copyright (C) 2016  Massimo Martelli <max213_88@hotmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "gui.h"
#include "connection.h"
#include "main_window.hpp"
#include "about_dialog.hpp"

#include <QtConcurrent>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QApplication>

static main_window *window = NULL;

int init_gui(int argc, char **argv)
{
	QApplication app(argc, argv);
	window = new main_window();
	window->show();
	int ret = app.exec();
	delete window;
	return ret;
}

main_window::main_window(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::main_window),
	current_mode(SINGLE_MODE),
	connection_status_message(connection_status_text[0])
{
	ui->setupUi(this);
	ui->slider->hide();
	ui->statusbar->addWidget(&connection_status_message);
	connect(this, SIGNAL(status_changed(const char*)), this,
		SLOT(queue_status_message(const char*)), Qt::QueuedConnection);
}

main_window::~main_window()
{
	delete ui;
}

enum mode main_window::get_current_mode() const
{
	return current_mode;
}

void main_window::set_current_mode(enum mode mode)
{
	current_mode = mode;
}

void main_window::on_action_about_triggered(bool checked)
{
	about_dialog diag(this);
	diag.exec();
}

void main_window::on_action_sync_boards_triggered(bool checked)
{
	queue_status_message("Sincronizzazione in corso...");

	QtConcurrent::run(this, &main_window::task_connect_boards);
}

void main_window::queue_status_message(const char *message)
{
	ui->statusbar->showMessage(message, STATUS_MESSAGE_DURATION);
}

void main_window::task_connect_boards()
{
	enum connection_manager_errorcodes errorcode = connect_boards();

	emit status_changed(connection_manager_errormessages[errorcode]);
}

void main_window::on_action_single_triggered(bool checked)
{
	ui->action_single->setChecked(true);
	ui->action_double->setChecked(false);

	current_mode = SINGLE_MODE;
}

void main_window::on_action_double_triggered(bool checked)
{
	ui->action_single->setChecked(false);
	ui->action_double->setChecked(true);

	current_mode = DOUBLE_MODE;
}

void main_window::on_action_toggle_cursor_triggered(bool checked)
{
	ui->gl->set_cursor_visible(checked);
}

void main_window::on_action_toggle_values_triggered(bool checked)
{
	ui->gl->set_values_visible(checked);
}

void main_window::on_action_toggle_axes_triggered(bool checked)
{
	ui->gl->set_axes_visible(checked);
}

void main_window::update_connection_status(enum connection_status status)
{
	if (status == connection_status)
		return;

	connection_status_message.setText(connection_status_text[status]);
	connection_status = status;
}

enum mode get_current_mode()
{
	return window->get_current_mode();
}

void set_current_mode(enum mode mode)
{
	window->set_current_mode(mode);
}

